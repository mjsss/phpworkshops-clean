#!/bin/bash

service apache2 restart

cd /code/Excercise4
composer install
bin/console doctrine:schema:update --force

tail -f /dev/null