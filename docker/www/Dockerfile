FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /code

RUN apt-get update \
    && apt-get install -y --no-install-recommends software-properties-common tzdata \
    && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       git \
       nginx \
       php7.3-pgsql \
       php7.3-fpm \
       php7.3-cli \
       php7.3-mbstring \
       php7.3-gd \
       php7.3-xml \
       php7.3-zip \
       php-xdebug \
       php7.3-curl \
       php-redis \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY xdebug.ini /etc/php/7.3/fpm/conf.d/00-xdebug.ini
COPY php.ini /etc/php/7.3/fpm/conf.d/00-api.ini
COPY entrypoint.sh /entrypoint.sh
COPY *.conf /etc/nginx/sites-available/
RUN rm /etc/nginx/sites-enabled/default \
    && ln -s /etc/nginx/sites-available/e1.conf /etc/nginx/sites-enabled/e1 \
    && ln -s /etc/nginx/sites-available/e2.conf /etc/nginx/sites-enabled/e2 \
    && ln -s /etc/nginx/sites-available/e3.conf /etc/nginx/sites-enabled/e3

RUN apt-get update && apt-get install curl
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN apt-get update && apt-get install wget
RUN wget -c http://static.phpmd.org/php/latest/phpmd.phar
RUN mv phpmd.phar /usr/local/bin/phpmd

EXPOSE 80

ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]