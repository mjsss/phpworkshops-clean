<?php

spl_autoload_register(function($name) {
    $path = explode('\\', $name);
    include array_pop($path).'.php';
});