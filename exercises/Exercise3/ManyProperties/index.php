<?php

namespace Exercise3\ManyProperties;

require_once '../../autoload.php';

$item = new Item();
$item
    ->setName('Test name')
    ->setCategory(2)
    ->setImage1('img1.jpg')
    ->setImage2('img2.jpg')
    ->setImage3('img3.jpg')
    ->setImage4('img4.jpg');