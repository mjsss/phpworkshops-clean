<?php

namespace Exercise3\ManyProperties;

class Item
{
    protected $id;
    protected $name;
    protected $category;
    protected $image1;
    protected $image2;
    protected $image3;
    protected $image4;

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setCategory(int $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setImage1(string $image): self
    {
        $this->image1 = $image;

        return $this;
    }

    public function getImage1()
    {
        return $this->image1;
    }

    public function setImage2(string $image): self
    {
        $this->image2 = $image;

        return $this;
    }

    public function getImage2(): string
    {
        return $this->image2;
    }

    public function setImage3(string $image): self
    {
        $this->image3 = $image;

        return $this;
    }

    public function getImage3(): string
    {
        return $this->image3;
    }

    public function setImage4(string $image): self
    {
        $this->image4 = $image;

        return $this;
    }

    public function getImage4(): string
    {
        return $this->image4;
    }

}