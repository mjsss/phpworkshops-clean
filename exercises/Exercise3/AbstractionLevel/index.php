<?php

namespace Exercise3\AbstractionLevel;

require_once '../../autoload.php';

$itemsController = new ItemsController();
$itemsController->listAction();
$itemsController->getAction(2);