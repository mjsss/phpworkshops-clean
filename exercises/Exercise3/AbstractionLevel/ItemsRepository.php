<?php

namespace Exercise3\AbstractionLevel;

class ItemsRepository
{
    protected $dbConnector;

    public function __construct()
    {
        $this->dbConnector = new Db();
    }

    public function getAll()
    {
        return $this->dbConnector->executeQuery('SELECT * FROM items');
    }

}