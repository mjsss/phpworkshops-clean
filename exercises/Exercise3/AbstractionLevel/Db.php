<?php

namespace Exercise3\AbstractionLevel;

class Db
{
    protected $items = [
        1 => 'Item 1',
        2 => 'Item 2',
        3 => 'Item 3',
        4 => 'Item 4'
    ];

    public function executeQuery(string $sql)
    {
        if ($sql === 'SELECT * FROM items') {
            return $this->items;
        }

        return $this->items[(int) substr($sql, -1, 1)];
    }
}