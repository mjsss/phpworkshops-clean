<?php

namespace Exercise3\AbstractionLevel;

class ItemsController
{

    protected $itemsRepository;

    public function __construct()
    {
        $this->itemsRepository = new ItemsRepository();
    }

    public function listAction()
    {
        var_dump($this->itemsRepository->getAll());
    }

    public function getAction($id)
    {
        $db = new Db();
        $db->executeQuery($id);
    }

}