<?php

namespace Exercise3\TemporaryVariable;

class Flight
{
    const TRIP_FACTOR = 0.2;
    const FUEL_FACTOR = 0.6;

    protected $baseFare;
    protected $taxRate;

    public function __construct($baseFare, $taxRate)
    {
        $this->baseFare = $baseFare;
        $this->taxRate = $taxRate;
    }

    public function calculatePrice(int $distance): array
    {
        $fuelPrice = $distance * self::FUEL_FACTOR * $this->baseFare;
        $tripPrice = $distance * self::TRIP_FACTOR * $this->baseFare;
        $tax = $this->taxRate * (
                $distance * self::FUEL_FACTOR * $this->baseFare
                + $distance * self::TRIP_FACTOR * $this->baseFare
            );

        return [
            'fuelPrice' => $fuelPrice,
            'tripPrice' => $tripPrice,
            'tax'       => $tax
        ];
    }
}