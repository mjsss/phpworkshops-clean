<?php

namespace Exercise3\Comments;

class InputText
{
    protected $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function show()
    {
        return '<input id="'.$this->id.'" type="text" />';
    }
}