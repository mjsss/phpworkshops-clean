<?php

namespace Exercise3\Naming;

class Shop
{
    protected $_db = [];

    public function add(int $iid, string $n)
    {
        $this->_db[$iid] = $n;
    }

    public function get(int $iid)
    {
        $i = $this->_db[$iid];
        echo $i;

        return $i;
    }

    public function inCollection(int $iid)
    {
        return array_key_exists($iid, $this->_db);
    }

    protected function _save(int $iid, string $n)
    {
        $this->_db[$iid] = $n;
    }

}