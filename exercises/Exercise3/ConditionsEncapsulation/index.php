<?php

namespace Exercise3\ConditionsEncapsulation;

require_once '../../autoload.php';

$taxCalculator = new TaxCalculator();
$item1 = new Item('Item name 1', 15, TaxCalculator::TAX_ZERO, 15);
var_dump($taxCalculator->getGrossPriceForItem($item1));
$item2 = new Item('Item name 2', 30, TaxCalculator::TAX_GROUP_1, 0.3);
var_dump($taxCalculator->getGrossPriceForItem($item2));
$item3 = new Item('Item name 3', 50, TaxCalculator::TAX_GROUP_1, 150);
var_dump($taxCalculator->getGrossPriceForItem($item3));
$item4 = new Item('Item name 4', 10000, TaxCalculator::TAX_GROUP_3, 7);
var_dump($taxCalculator->getGrossPriceForItem($item4));
$item5 = new Item('Item name 5', 237, TaxCalculator::TAX_GROUP_2, 49);
var_dump($taxCalculator->getGrossPriceForItem($item5));