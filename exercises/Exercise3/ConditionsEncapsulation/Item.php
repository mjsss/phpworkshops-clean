<?php

namespace Exercise3\ConditionsEncapsulation;

class Item
{
    protected $price;
    protected $taxGroup;
    protected $weight;
    protected $name;

    public function __construct(string $name, int $price, int $taxGroup, float $weight)
    {
        $this->name = $name;
        $this->price = $price;
        $this->taxGroup = $taxGroup;
        $this->weight = $weight;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getTaxGroup()
    {
        return $this->taxGroup;
    }

    public function getWeight()
    {
        return $this->weight;
    }
}