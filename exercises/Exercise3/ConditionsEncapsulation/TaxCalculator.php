<?php

namespace Exercise3\ConditionsEncapsulation;

class TaxCalculator
{
    const TAX_ZERO = 0;
    const TAX_GROUP_1 = 0.12;
    const TAX_GROUP_2 = 0.17;
    const TAX_GROUP_3 = 0.25;
    const TAX_GROUP_EXTRA = 0.03;

    public function getGrossPriceForItem(Item $item)
    {
        if ($item->getTaxGroup() === self::TAX_ZERO || ($item->getWeight() < 0.5 && $item->getTaxGroup() === self::TAX_GROUP_1)) {
            return $item->getPrice();
        }

        if ($item->getTaxGroup() === self::TAX_GROUP_1 && $item->getWeight() > 100) {
            return (self::TAX_GROUP_1 + self::TAX_GROUP_EXTRA) * $item->getPrice();
        }

        if ($item->getTaxGroup() === self::TAX_GROUP_3 && $item->getPrice() > 1000) {
            return self::TAX_GROUP_1 + $item->getPrice();
        }

        switch ($item->getTaxGroup()) {
            case self::TAX_GROUP_1:
                return self::TAX_GROUP_1 * $item->getPrice();
            case self::TAX_GROUP_2:
                return self::TAX_GROUP_2 * $item->getPrice();
            case self::TAX_GROUP_3:
                return self::TAX_GROUP_3 * $item->getPrice();
        }
    }

}