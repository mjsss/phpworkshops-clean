<?php

namespace Exercise3\ErrorHandling;

class Tokenizer
{
    public function getAsArray(string $token)
    {
        if (strlen($token) < 10) {
            return 1;
        }
        if (substr($token, 0, 1) === 'a') {
            return 2;
        }
        if (substr_count($token, ',') < 2) {
            return 3;
        }

        return explode(',', $token);
    }
}