<?php

namespace Exercise3\DependencyInjection;

class Post
{
    protected $purifier;

    public function setPurifier(Purifier $purifier)
    {
        $this->purifier = $purifier;
    }

    public function show(string $text)
    {
        echo $this->purifier->purify($text);
    }
}