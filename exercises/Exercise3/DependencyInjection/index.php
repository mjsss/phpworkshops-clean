<?php

namespace Exercise3\DependencyInjection;

require_once '../../autoload.php';

$post = new Post();
$post->setPurifier(new Purifier());
$post->show('<b>Test</b>');