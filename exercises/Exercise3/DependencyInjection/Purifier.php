<?php

namespace Exercise3\DependencyInjection;

class Purifier
{
    public function purify(string $text)
    {
        return htmlentities($text);
    }
}
