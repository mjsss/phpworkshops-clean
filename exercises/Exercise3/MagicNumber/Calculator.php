<?php

namespace Exercise3\MagicNumber;

class Calculator
{
    public function calculateWorkTimeInSeconds($numWorkingHours)
    {
        return $numWorkingHours * 3600;
    }
}