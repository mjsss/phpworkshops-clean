<?php

namespace Exercise3\LogicalArgument;

require_once '../../autoload.php';

$warehouse = new Warehouse([
    5 => 'Item five',
    3 => 'Item three',
    8 => 'Item eight'
]);

echo $warehouse->getItem(5, false);
echo $warehouse->getItem(3, true);
echo $warehouse->getItem(3, false);