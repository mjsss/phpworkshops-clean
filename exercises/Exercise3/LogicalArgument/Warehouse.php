<?php

namespace Exercise3\LogicalArgument;

class Warehouse
{

    protected $items = [];

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function getItem(int $id, bool $remove)
    {
        if (!array_key_exists($id, $this->items)) {
            throw new \Exception('Item does not exist');
        }

        $item = $this->items[$id];
        if ($remove) {
            unset($this->items[$id]);
        }

        return $item;
    }

}