<?php

namespace Exercise3\OutboundArguments;

require_once '../../autoload.php';

$encryptor = new Encryptor();
$password = 'qwerty';
var_dump($encryptor->generateHash($password));
var_dump($password);