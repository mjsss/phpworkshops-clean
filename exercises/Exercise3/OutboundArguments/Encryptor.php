<?php

namespace Exercise3\OutboundArguments;

class Encryptor
{

    public function generateHash(string &$hash)
    {
        if (strlen($hash) < 7) {
            return false;
        }

        $hash = md5($hash);

        return true;
    }

}