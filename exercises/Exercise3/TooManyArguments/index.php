<?php

namespace Exercise3\TooManyArguments;

require_once '../../autoload.php';

$shop = new Shop();
$shop->order(
    'Jan',
    'Kowalski',
    '66644433355',
    'jan.kowalski@example.com',
    '45121156978',
    'Street name',
    'Lodz',
    '11-123',
    'Poland'
);