<?php

namespace Exercise3\TooManyArguments;

class Shop
{
    public function order(
        string $firstname,
        string $lastname,
        string $phone,
        string $email,
        string $pesel,
        string $street,
        string $city,
        string $zip,
        string $country
    )
    {
        echo 'Processing order';
    }
}