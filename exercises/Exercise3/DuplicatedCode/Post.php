<?php

namespace Exercise3\DuplicatedCode;

class Post
{
    protected $author;
    protected $body;

    public function __construct(string $author, string $body)
    {
        $this->author = $author;
        $this->body = $body;
    }

    public function showFullPost()
    {
        $text = '<p><a href="/author/name/'.$this->author.'">'.$this->author.'</a></p><p>'.$this->body.'</p>';

        return '<div>'.$text.'</div>';
    }

    public function showSummary()
    {
        $text = '<p><a href="/author/name/'.$this->author.'">'.$this->author.'</a></p><p>'.substr($this->body, 0, 10).'</p>';

        return '<div>'.$text.'</div>';
    }

    public function showOnlyAuthor()
    {
        $text = '<p><a href="/author/name/'.$this->author.'">'.$this->author.'</a></p>';

        return '<div>'.$text.'</div>';
    }
}