<?php

namespace Exercise3\DuplicatedCode;

require_once '../../autoload.php';

$post = new Post('michal', 'This is my first post in this forum');
$post->showFullPost();
$post->showOnlyAuthor();
$post->showSummary();